FROM ubuntu:20.04

USER root

ARG DEBIAN_FRONTEND=noninteractive

ARG KUBECTL_VERSION
ENV KUBECTL_VERSION=$KUBECTL_VERSION

RUN set -x && \
    apt-get update && \
    apt-get install -y \
    apt-transport-https \
    ca-certificates \
    build-essential \
    net-tools \
    curl \
    python3 \
    python3-dev \
    python3-pip \
    jq \
    gettext-base && \
    rm -rf /var/lib/apt/lists/*

RUN curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 && \
    chmod 700 get_helm.sh && \
    ./get_helm.sh

RUN pip3 install -U --no-cache-dir setuptools shyaml
RUN ln -sf /usr/bin/python3 /usr/bin/python && ln -sf /usr/bin/pip3 /usr/bin/pip

RUN set -x && \
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - && \
    echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | tee -a /etc/apt/sources.list.d/kubernetes.list

ADD https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl /usr/bin/kubectl

RUN set -x && \
    chmod +x /usr/bin/kubectl

RUN set -x && \
    curl --version && \
    envsubst --version && \
    python --version && \
    pip --version && \
    shyaml --version && \
    kubectl version --client && \
    helm version

ENTRYPOINT ["/bin/bash", "-c"]

